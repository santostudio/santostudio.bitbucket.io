const getDate = new Date();
document.getElementById("crText").innerHTML = `Copyrights @ ${getDate.getFullYear()} | Santo Studio`;

function showImages(data) {
    document.getElementById('loaderDiv').style.display = 'block';
    const getImagesURL = firebase.database().ref(`${data}`);
    const oldImages = document.getElementById("imageContainer");
    if (oldImages !== null) {
        while (oldImages.firstChild) {
            oldImages.removeChild(oldImages.lastChild);
        }
    }
    getImagesURL.on("value", snap => {
        const getImagesFromDB = [];
        let indivKeys;
        let indivImage;
        // const getImages = firebase.storage().ref();
        // const imagesRef = getImages.child(data);
        getImagesFromDB.push(snap.val());
        for (var key in getImagesFromDB) {
            if (getImagesFromDB.hasOwnProperty(key)) {
                indivImage = getImagesFromDB[key];
                indivImage = Object.values(indivImage);
                indivKeys = Object.keys(getImagesFromDB[key]);
            }
        }
        let i = 0;
        for (var key in indivImage) {
            const colorPallete = ['rgb(158, 182, 191)', 'rgb(81, 71, 67)', 'rgb(60, 69, 68)', 'rgb(168, 183, 187)', 'rgb(88, 87, 68)', 'rgb(162, 155, 148)', 'rgb(188, 193, 186)', 'rgb(240, 182, 200)', 'rgb(182, 215, 240)', 'rgb(162, 154, 125)', 'rgb(223, 215, 215)']
            const randomColor = Math.floor(Math.random()* 10);
            if (indivImage.hasOwnProperty(key)) {
                i += 1;
                const values = indivKeys[key];
                // const images = imagesRef.child(indivImage[key]);
                const images = indivImage[key];
                // const zips = imagesRef.child(`${indivImage[key]}.zip`);
                // images.getDownloadURL().then(function(url) {
                    // zips.getDownloadURL().then(function(zipUrl) {
                const createImg = document.createElement('div');
                createImg.className = "imgs";
                createImg.id = `imgs`;
                createImg.className = 'imgs';
                createImg.style.backgroundColor = colorPallete[randomColor];
                createImg.style.backgroundImage = `url(./${data}/${images})`;
                const textDiv = document.createElement('text');
                textDiv.id = 'textDiv';
                textDiv.innerHTML = values;
                textDiv.style.position = "relative";
                textDiv.style.fontFamily = "Open Sans";
                textDiv.style.fontSize = "14px";
                textDiv.style.fontWeight = "400";
                textDiv.style.textAlign = "center";
                createImg.appendChild(textDiv);
                const downloadImg = document.createElement('a');
                downloadImg.id = 'download';
                downloadImg.download;
                downloadImg.style.cursor = 'pointer';
                // downloadImg.href = zipUrl;
                downloadImg.href = `./${data}/${images}.zip`;
                downloadImg.download;
                const downloadIcon = document.createElement('i');
                downloadIcon.className = "lni lni-download";
                downloadIcon.addEventListener('click', function () {
                    firebase.database().ref("downloads")
                    .once('value')
                    .then(
                        (function(snapshot) {
                            let counts = snapshot.val().counts;
                            firebase.database().ref("downloads").set({counts: parseInt(counts) + 1});
                        })
                    )
                });
                downloadImg.appendChild(downloadIcon);
                const textExpand = document.createElement('i');
                textExpand.id = 'textExpand';
                textExpand.className = "lni lni-frame-expand";
                textExpand.style.cursor = 'pointer';
                textExpand.addEventListener('click', function () {
                    document.getElementById('infoContainer').style.display = 'block';
                    document.getElementById('showImgClk').src = `./${data}/${images}`;
                });
                createImg.appendChild(downloadImg);
                createImg.appendChild(textExpand);
                document.getElementById('imageContainer').appendChild(createImg);
                // });
            // });
        }
        setTimeout(() => {
            document.getElementById('loaderDiv').style.display = 'none';
        }, 1500);
        }   
    });
}
document.getElementById('loaderDiv').style.display = 'block';
showImages('landscape');
document.getElementById('landscapes').style.color = '#3473F9';
document.getElementById('landscapes').style.borderBottom = '3px solid #3473F9';
document.getElementById('architecture').addEventListener('click', function () {
    document.getElementById('architecture').style.color = '#3473F9';
    document.getElementById('architecture').style.borderBottom = '3px solid #3473F9';
    document.getElementById('landscapes').style.color = 'rgb(104, 106, 107)';
    document.getElementById('flowers').style.color = 'rgb(104, 106, 107)';
    document.getElementById('pets').style.color = 'rgb(104, 106, 107)';
    document.getElementById('landscapes').style.borderBottom = '3px solid white';
    document.getElementById('flowers').style.borderBottom = '3px solid white';
    document.getElementById('pets').style.borderBottom = '3px solid white';
});
document.getElementById('landscapes').addEventListener('click', function () {
    document.getElementById('landscapes').style.color = '#3473F9';
    document.getElementById('landscapes').style.borderBottom = '3px solid #3473F9';
    document.getElementById('architecture').style.color = 'rgb(104, 106, 107)';
    document.getElementById('flowers').style.color = 'rgb(104, 106, 107)';
    document.getElementById('pets').style.color = 'rgb(104, 106, 107)';
    document.getElementById('architecture').style.borderBottom = '3px solid white';
    document.getElementById('flowers').style.borderBottom = '3px solid white';
    document.getElementById('pets').style.borderBottom = '3px solid white';
});
document.getElementById('flowers').addEventListener('click', function () {
    document.getElementById('flowers').style.color = '#3473F9';
    document.getElementById('flowers').style.borderBottom = '3px solid #3473F9';
    document.getElementById('landscapes').style.color = 'rgb(104, 106, 107)';
    document.getElementById('architecture').style.color = 'rgb(104, 106, 107)';
    document.getElementById('pets').style.color = 'rgb(104, 106, 107)';
    document.getElementById('landscapes').style.borderBottom = '3px solid white';
    document.getElementById('architecture').style.borderBottom = '3px solid white';
    document.getElementById('pets').style.borderBottom = '3px solid white';
});
document.getElementById('pets').addEventListener('click', function () {
    document.getElementById('pets').style.color = '#3473F9';
    document.getElementById('pets').style.borderBottom = '3px solid #3473F9';
    document.getElementById('landscapes').style.color = 'rgb(104, 106, 107)';
    document.getElementById('flowers').style.color = 'rgb(104, 106, 107)';
    document.getElementById('architecture').style.color = 'rgb(104, 106, 107)';
    document.getElementById('landscapes').style.borderBottom = '3px solid white';
    document.getElementById('flowers').style.borderBottom = '3px solid white';
    document.getElementById('architecture').style.borderBottom = '3px solid white';
});